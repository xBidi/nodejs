# nodejs
Project template for nodejs

code style
````
https://raw.githubusercontent.com/google/styleguide/gh-pages/eclipse-java-google-style.xml
````
package manager
````
npm
````
modules install
````
npm install
````
launch command
````
node app/server.js
nodemon app/server.js
````
launch server docker
````
docker image build -t nodejs .
docker stop nodejs
docker rm nodejs
docker run -p 80:80 --name nodejs -d nodejs
docker logs -f nodejs

````
example route
````
http://localhost:80/
````
launch server on heroku
````
heroku login
heroku create
git push heroku master
heroku ps:scale web=1
heroku open
heroku logs --tail
````
