'use strict';
module.exports = {
    api
};

async function api(request, response) {
    const params = request.params; // require param name in routes.js, using ":paramExample" for this example
    const query = request.query;
    const body = request.body;
    const method = request.method;
    const headers = request.headers;
    await response.json({params, query, body, method, headers});
}