"use strict";
const express = require('express');
const app = express();

/**
 * NodeJs examples
 */
require("./example/example");

setRequestSizeLimit(app);
setMiddleWare(app);
setRoutes(app);
startServer(app);

function setRequestSizeLimit(app) {
    const bodyParser = require('body-parser');
    app.use(bodyParser.json({limit: "50mb", extended: true}));
    app.use(bodyParser.urlencoded({limit: "50mb", extended: true}));
}

function setMiddleWare(app) {
    const logger = require("./middleware/logger");
    app.use("/", logger);
    const cors = require("./middleware/cors");
    app.use("/", cors);
}

function setRoutes(app) {
    const apiRoutes = require("./routes/apiRoutes")
    app.use("/", apiRoutes);
}

function startServer(app) {
    const port = process.env.PORT || 80;
    app.listen(port, function () {
        console.log('Server:', `Server started on port: ${port}`)
    });
}
