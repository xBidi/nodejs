'use strict';
const express = require("express");
const router = express.Router();
router.use(function (req, res, next) {
    console.log("Server:", `request received on ${req.url}`);
    next();
});
module.exports=router;
