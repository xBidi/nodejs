'use strict';
const express = require("express");
const router = express.Router();
const apiController = require("../controller/apiController");

router.get("/", apiController.api);
router.get("/:paramExample", apiController.api);
router.get("/:paramExample1/:paramExample2", apiController.api);
router.post("/", apiController.api);
router.put("/", apiController.api);
router.delete("/", apiController.api);

module.exports = router;

