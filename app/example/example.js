'use strict';
runExamples().then(value => {
    console.log("EXAMPLES RESULT: ", value);
}).catch(reason => {
    console.log("EXAMPLES FAILED: ", reason);
});

/**
 * Response with all the examples
 */
async function runExamples() {
    const parallelFunctionsResponse = await parallelFunctions();
    const copyVariableResponse = copyVariable();
    const asyncToSyncResponse = await asyncToSync();
    exampleLoops();
    const exampleClassResponse = exampleClass();
    const jsonKeys = getKeysFromJson();
    const [variable1, variable2, variable3] = returnMultipleVariables();
    removeJsonKey();
    jsonToString();
    stringToJson();
    return {
        parallelFunctionsResponse,
        copyVariableResponse,
        asyncToSyncResponse,
        jsonKeys,
        exampleClassResponse,
        exampleReturnMultipleVariables: [variable1, variable2, variable3],
    };
}

/**
 * Json to string and string to json
 */
function jsonToString() {
    const exampleJson = {name: "name", description: "description"};
    const jsonToString = JSON.stringify(exampleJson);
    console.log(`jsonToString: ${jsonToString}`);
}

/**
 * String to json
 */
function stringToJson() {
    const exampleString = "{\"name\":\"name\",\"description\":\"description\"}";
    const stringToJson = JSON.parse(exampleString);
    console.log(`stringToJson: ${stringToJson}`);
}

/**
 * Remove key from json
 */
function removeJsonKey() {
    const exampleJson = {
        name: "name",
        description: "description",
    };
    console.log(`exampleJson before removeKey: ${JSON.stringify(exampleJson)}`);
    delete exampleJson.description;
    console.log(`exampleJson after removeKey: ${JSON.stringify(exampleJson)}`);
}

/**
 * Return multiple variables
 */
function returnMultipleVariables() {
    const variable1 = "variable1";
    const variable2 = "variable2";
    const variable3 = "variable3";
    return [variable1, variable2, variable3];
}

/**
 * Example class
 */
function exampleClass() {
    const example = new Example(
        "nameExample",
        "descriptionExample",
    );
    return example;
}

class Example {
    constructor(name, description) {
        this.name = name;
        this.description = description;
    }
}

/**
 * Get keys from json
 */
function getKeysFromJson() {
    let json = {
        name: "exampleObject",
        description: "exampleDescription",
    };
    return Object.keys(json);
}

/**
 * Example for each loop
 */
function exampleLoops() {
    const exampleObject = {name: "exampleObject", description: "exampleDescription"};
    const exampleArray = [{...exampleObject}, {...exampleObject}, {...exampleObject}];
    forExample();
    forInExample(exampleObject);
    forOfExample(exampleArray);
    whileExample();
    doWhileExample();
}

function doWhileExample() {
    console.log("DO WHILE EXAMPLE");
    let continueDoWhile = false;
    do {
        console.log("inside doWhile");
    } while (continueDoWhile);
}

function whileExample() {
    console.log("WHILE EXAMPLE");
    let continueWhile = false;
    while (continueWhile) {
        console.log("inside while");
    }
}

function forOfExample(array) {
    console.log("FOR OF EXAMPLE");
    for (let object of array) {
        console.log(object);
    }
}

function forInExample(object) {
    console.log("FOR IN EXAMPLE");
    for (let attribute in object) {
        console.log(attribute);
    }
}

function forExample() {
    console.log("FOR I EXAMPLE");
    for (let i = 0; i < 5; i++) {
        console.log(i);
    }
}

/**
 * Async function to sync
 */
async function asyncToSync() {
    return await new Promise((resolve, reject) => {
        exampleAsyncFunction().then(value => {
            resolve(value); // result ok
        }).catch(reason => {
            reject(reason); // result fail (throws exception)
        });
    });
}

async function exampleAsyncFunction() {
    return "async response";
}

/**
 * Wait all functions to be completed
 */
async function parallelFunctions() {
    let promises = [];
    promises.push(getMessage("function1"));
    promises.push(getMessage("function2"));
    promises.push(getMessage("function3"));
    let [function1, function2, function3] = await Promise.all(promises);
    return {function1, function2, function3};
}

async function getMessage(message) {
    return message;
}

/**
 * Copy the value of the variable
 */
function copyVariable() {
    const example1var = {example: "defaultValue"};
    const example1varCopy = {example1var}; // difference
    example1var.example = "valueChange";

    const example2var = {example: "defaultValue"};
    const example2varCopy = {...example2var}; // difference
    example2var.example = "valueChange";
    return {
        example1: {variable: JSON.stringify(example1var), variableCopy: JSON.stringify(example1varCopy)},
        example2: {variable: JSON.stringify(example2var), variableCopy: JSON.stringify(example2varCopy)},
    };
}
